#!/bin/bash
## Author: Michael Ramsey
## Objective Find A cPanel Users Domlogs Stats for last 5 days for all of their domains.
## https://gitlab.com/cpaneltoolsnscripts/apache-domlogs-by-cpanel-user
## How to use.
# ./ApacheDomlogsBycPanelUser.sh username
#./ApacheDomlogsBycPanelUser.sh exampleuserbob
#
##bash <(curl https://gitlab.com/cpaneltoolsnscripts/apache-domlogs-by-cpanel-user/raw/master/ApacheDomlogsBycPanelUser.sh || wget -O - https://gitlab.com/cpaneltoolsnscripts/apache-domlogs-by-cpanel-user/raw/master/ApacheDomlogsBycPanelUser.sh) exampleuserbob;
##
Username=$1

CURRENTDATE=$(date +"%Y-%m-%d %T") # 2019-02-09 06:47:56
PreviousDay1=$(date --date='1 day ago' +"%Y-%m-%d")  # 2019-02-08
PreviousDay2=$(date --date='2 days ago' +"%Y-%m-%d") # 2019-02-07
PreviousDay3=$(date --date='3 days ago' +"%Y-%m-%d") # 2019-02-06
PreviousDay4=$(date --date='4 days ago' +"%Y-%m-%d") # 2019-02-05

datetimeDom=$(date +"%d/%b/%Y") # 09/Feb/2019
datetimeDom1DaysAgo=$(date --date='1 day ago' +"%d/%b/%Y")  # 08/Feb/2019
datetimeDom2DaysAgo=$(date --date='2 days ago' +"%d/%b/%Y") # 07/Feb/2019
datetimeDom3DaysAgo=$(date --date='3 days ago' +"%d/%b/%Y") # 06/Feb/2019
datetimeDom4DaysAgo=$(date --date='4 days ago' +"%d/%b/%Y") # 05/Feb/2019

main_function() {
echo "============================================================="
echo "Apache Dom Logs POST Requests for ${CURRENTDATE} for $Username"
sudo grep -r "$datetimeDom" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs GET Requests for ${CURRENTDATE} for $Username"
sudo grep -r "$datetimeDom" /usr/local/apache/domlogs/"$Username" | grep GET | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g" | sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs Top 10 bot/crawler requests per domain name for $(date +"%Y-%m-%d")"
sudo grep -r "$datetimeDom" /usr/local/apache/domlogs/"$Username" | egrep -i '(crawl|bot|spider|yahoo|bing|google)'| awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs top ten IPs for ${CURRENTDATE} for $Username"

command=$(sudo grep -r "$datetimeDom" /usr/local/apache/domlogs/"${Username}" | grep POST | awk '{print $1}' | cut -d: -f2 | sort | uniq -c | sort -rn | head);readarray -t iparray < <( echo "${command}" | tr '/' '\n'); echo ""; for IP in "${iparray[@]}"; do echo "$IP"; done; echo ""; echo "Show unique IP's with whois IP, Country,and ISP"; echo ""; for IP in "${iparray[@]}"; do IP=$(echo "$IP" |grep -Eo '([0-9]{1,3}[.]){3}[0-9]{1,3}|(*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?\s*)'); whois -h whois.cymru.com " -c -p $IP"|cut -d"|" -f 2,4,5|grep -Ev 'IP|whois.cymru.com'; done

echo ""
echo "Apache Dom Logs find the top number of uri's being requested for $(date +"%Y-%m-%d")"
sudo grep -r "$datetimeDom" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $7}' | cut -d: -f2 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g" | sort | uniq -c | sort -rn | head
echo "============================================================="

#Past few days stats
echo "Apache Dom Logs POST Requests for ${PreviousDay1} for $Username"
sudo grep -r "$datetimeDom1DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g" | sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs GET Requests for ${PreviousDay1} for $Username"
sudo grep -r "$datetimeDom1DaysAgo" /usr/local/apache/domlogs/"$Username" | grep GET | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g" | sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs Top 10 bot/crawler requests per domain name for ${PreviousDay1}"
sudo grep -r "$datetimeDom1DaysAgo" /usr/local/apache/domlogs/"$Username" | egrep -i '(crawl|bot|spider|yahoo|bing|google)'| awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g" | sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs top ten IPs for ${PreviousDay1} for $Username"

command=$(sudo grep -r "$datetimeDom1DaysAgo" /usr/local/apache/domlogs/"${Username}" | grep POST | awk '{print $1}' | cut -d: -f2 | sort | uniq -c | sort -rn | head);readarray -t iparray < <( echo "${command}" | tr '/' '\n'); echo ""; for IP in "${iparray[@]}"; do echo "$IP"; done; echo ""; echo "Show unique IP's with whois IP, Country,and ISP"; echo ""; for IP in "${iparray[@]}"; do IP=$(echo "$IP" |grep -Eo '([0-9]{1,3}[.]){3}[0-9]{1,3}|(*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?\s*)'); whois -h whois.cymru.com " -c -p $IP"|cut -d"|" -f 2,4,5|grep -Ev 'IP|whois.cymru.com'; done

echo ""
echo "Apache Dom Logs find the top number of uri's being requested for ${PreviousDay1}"
sudo grep -r "$datetimeDom1DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $7}' | cut -d: -f2 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo "============================================================="
echo "Apache Dom Logs POST Requests for ${PreviousDay2} for $Username"
sudo grep -r "$datetimeDom2DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs GET Requests for ${PreviousDay2} for $Username"
sudo grep -r "$datetimeDom2DaysAgo" /usr/local/apache/domlogs/"$Username" | grep GET | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs Top 10 bot/crawler requests per domain name for ${PreviousDay2}"
sudo grep -r "$datetimeDom2DaysAgo" /usr/local/apache/domlogs/"$Username" | egrep -i '(crawl|bot|spider|yahoo|bing|google)'| awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs top ten IPs for ${PreviousDay2}"

command=$(sudo grep -r "$datetimeDom2DaysAgo" /usr/local/apache/domlogs/"${Username}" | grep POST | awk '{print $1}' | cut -d: -f2 | sort | uniq -c | sort -rn | head);readarray -t iparray < <( echo "${command}" | tr '/' '\n'); echo ""; for IP in "${iparray[@]}"; do echo "$IP"; done; echo ""; echo "Show unique IP's with whois IP, Country,and ISP"; echo ""; for IP in "${iparray[@]}"; do IP=$(echo "$IP" |grep -Eo '([0-9]{1,3}[.]){3}[0-9]{1,3}|(*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?\s*)'); whois -h whois.cymru.com " -c -p $IP"|cut -d"|" -f 2,4,5|grep -Ev 'IP|whois.cymru.com'; done


echo ""
echo "Apache Dom Logs find the top number of uri's being requested for ${PreviousDay2}"
sudo grep -r "$datetimeDom2DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $7}' | cut -d: -f2 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo "============================================================="
echo "Apache Dom Logs POST Requests for ${PreviousDay3} for $Username"
sudo grep -r "$datetimeDom3DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs GET Requests for ${PreviousDay3} for $Username"
sudo grep -r "$datetimeDom3DaysAgo" /usr/local/apache/domlogs/"$Username" | grep GET | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs Top 10 bot/crawler requests per domain name for ${PreviousDay3}"
sudo grep -r "$datetimeDom3DaysAgo" /usr/local/apache/domlogs/"$Username" | egrep -i '(crawl|bot|spider|yahoo|bing|google)'| awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs top ten IPs for ${PreviousDay3} for $Username"

command=$(sudo grep -r "$datetimeDom3DaysAgo" /usr/local/apache/domlogs/"${Username}" | grep POST | awk '{print $1}' | cut -d: -f2 | sort | uniq -c | sort -rn | head);readarray -t iparray < <( echo "${command}" | tr '/' '\n'); echo ""; for IP in "${iparray[@]}"; do echo "$IP"; done; echo ""; echo "Show unique IP's with whois IP, Country,and ISP"; echo ""; for IP in "${iparray[@]}"; do IP=$(echo "$IP" |grep -Eo '([0-9]{1,3}[.]){3}[0-9]{1,3}|(*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?\s*)'); whois -h whois.cymru.com " -c -p $IP"|cut -d"|" -f 2,4,5|grep -Ev 'IP|whois.cymru.com'; done

echo ""
echo "Apache Dom Logs find the top number of uri's being requested for ${PreviousDay3}"
sudo grep -r "$datetimeDom3DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $7}' | cut -d: -f2 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo "============================================================="
echo "Apache Dom Logs POST Requests for ${PreviousDay4} for $Username"
sudo grep -r "$datetimeDom4DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs GET Requests for ${PreviousDay4} for $Username"
sudo grep -r "$datetimeDom4DaysAgo" /usr/local/apache/domlogs/"$Username" | grep GET | awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs Top 10 bot/crawler requests per domain name for ${PreviousDay4}"
sudo grep -r "$datetimeDom4DaysAgo" /usr/local/apache/domlogs/"$Username" | egrep -i '(crawl|bot|spider|yahoo|bing|google)'| awk '{print $1}' | cut -d: -f1 |sed 's|/usr/local/apache/domlogs/||g'|sed 's|-ssl_log||g' |sed "s|$Username/||g"| sort | uniq -c | sort -rn | head
echo ""
echo "Apache Dom Logs top ten IPs for ${PreviousDay4} for $Username"

command=$(sudo grep -r "$datetimeDom4DaysAgo" /usr/local/apache/domlogs/"${Username}" | grep POST | awk '{print $1}' | cut -d: -f2 | sort | uniq -c | sort -rn | head);readarray -t iparray < <( echo "${command}" | tr '/' '\n'); echo ""; for IP in "${iparray[@]}"; do echo "$IP"; done; echo ""; echo "Show unique IP's with whois IP, Country,and ISP"; echo ""; for IP in "${iparray[@]}"; do IP=$(echo "$IP" |grep -Eo '([0-9]{1,3}[.]){3}[0-9]{1,3}|(*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?\s*)'); whois -h whois.cymru.com " -c -p $IP"|cut -d"|" -f 2,4,5|grep -Ev 'IP|whois.cymru.com'; done


echo ""
echo "Apache Dom Logs find the top number of uri's being requested for ${PreviousDay4}"
sudo grep -r "$datetimeDom4DaysAgo" /usr/local/apache/domlogs/"$Username" | grep POST | awk '{print $7}' | cut -d: -f2 | sort | uniq -c | sort -rn | head
echo "============================================================="
}

# log everything, but also output to stdout
main_function 2>&1 | tee -a "$1"-Apachelogs.txt
